# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
import tempfile

from packmaker import packdef, packlock
from packmaker.framew.application import OperationError

##############################################################################

reference_packdef = """
---
name:  referencepack
title: Reference Pack
version: ${MODPACK_VERSION:-dev}
authors:
  - mcrewson

icon: icon.png
news: news.html

minecraft: 1.12.2
forge: 14.23.5.2854

mods:
  - jei
  - journeymap
  - mantle
  - tinkers-construct:
      release: "TConstruct-1.12.2-2.13.0.183.jar"

resourcepacks:
  - faithful-32x
  - soartex-fanver-vanilla:

files:
  - location: src/

routhio:
  html:
    - location: html/
  launch:
    flags:
      - "-Dfml.ignoreInvalidMinecraftCertificates=true -XX:+UseG1GC -XX:UseSSE=3"
"""

##############################################################################


class TestPackDef (object):

    def generate_packdef(self, packdef_string):
        with tempfile.NamedTemporaryFile(mode="wt") as f:
            f.write(packdef_string)
            f.flush()
            pack = packdef.PackDefinition([f.name])
            pack.load()
            return pack

    def test_simpledef(self):
        pack = self.generate_packdef(reference_packdef)
        assert isinstance(pack, packdef.PackDefinition)
        assert pack.name is not None
        assert pack.title is not None
        assert pack.version is not None
        assert len(pack.authors) > 0 and pack.authors[0] not in ("", None)
        assert pack.minecraft_version is not None
        assert pack.forge_version is not None
        assert len(pack.mods) > 0
        assert len(pack.resourcepacks) > 0
        assert len(pack.files) > 0
        assert pack.get_mod('jei') is not None
        assert len(pack.get_all_mods()) == 4
        assert pack.get_resourcepack('faithful-32x') is not None
        assert len(pack.get_all_resourcepacks()) == 2

    def test_missingfields(self):
        pack = self.generate_packdef("---\nname: missingpack\n")
        assert isinstance(pack, packdef.PackDefinition)
        assert pack.name is not None
        assert pack.title is None

    def test_extrafields(self):
        pack = self.generate_packdef("""
---
name: extrapack
title: Extra Pack
version: 0

extrafield:
  - ignoreme
  - helloworld
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert pack.name is not None
        with pytest.raises(AttributeError):
            assert pack.extrafield is None

    def test_metadata_are_strings(self):
        pack = self.generate_packdef("""
---
name: metadata
title: Metadata Pack
version: 0

authors:
  - myname
  - 123
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert type(pack.name) is str
        assert type(pack.title) is str
        assert type(pack.version) is str
        assert type(pack.authors) is list
        for author in pack.authors:
            assert type(author) is str

    def test_author_string(self):
        pack = self.generate_packdef("""
---
name: authorpack
title: Author Pack
version: 0

author: myname
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert type(pack.authors) is list
        assert len(pack.authors) == 1
        assert pack.authors[0] == 'myname'

    def test_author_list(self):
        pack = self.generate_packdef("""
---
name: authorpack
title: Author Pack
version: 0

author:
  - myname
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert type(pack.authors) is list
        assert len(pack.authors) == 1
        assert pack.authors[0] == 'myname'

    def test_authors_string(self):
        pack = self.generate_packdef("""
---
name: authorpack
title: Author Pack
version: 0

authors: myname
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert type(pack.authors) is list
        assert len(pack.authors) == 1
        assert pack.authors[0] == 'myname'

    def test_authors_list(self):
        pack = self.generate_packdef("""
---
name: authorpack
title: Author Pack
version: 0

authors:
  - myname
""")
        assert isinstance(pack, packdef.PackDefinition)
        assert type(pack.authors) is list
        assert len(pack.authors) == 1
        assert pack.authors[0] == 'myname'

    def test_invalidfilename(self):
        with pytest.raises(OperationError):
            p = packdef.PackDefinition(["nosuchfile.yml"])
            p.load()

    def test_invalidyaml1(self):
        with pytest.raises(OperationError):
            self.generate_packdef("""
---
name: invalidyaml
mods:
  - amod
  invalidhere
""")

    def test_invalidyaml2(self):
        with pytest.raises(OperationError):
            self.generate_packdef("""
---
name: invalidyaml
mods:
  - amod

    resourcepacks:
      - faithful-32x
""")

    def test_invalidyaml3(self):
        with pytest.raises(OperationError):
            self.generate_packdef("""
---
name: invalidyaml
mods:
  - amod:
    serveronly: true
""")

    def test_generate_packlock(self):
        pack = self.generate_packdef(reference_packdef)
        lock = pack.get_packlock()
        assert isinstance(lock, packlock.PackLock)

    def test_moddefinition(self):
        pack = self.generate_packdef("""
---
name: moddef
mods:
  - somemod:
      release: "somemod-1.2.3.jar"
      optional: false
      recommendation: starred
      selected: false
      clientonly: false
      serveronly: false
""")
        assert isinstance(pack, packdef.PackDefinition)
        mod = pack.get_mod('somemod')
        assert isinstance(mod, packdef.ModDefinition)
        assert mod.version == "somemod-1.2.3.jar"
        assert mod.optional is False
        assert mod.recommendation == 'starred'
        assert mod.selected is False
        assert mod.clientonly is False
        assert mod.serveronly is False

    def test_moddefinition_extrafields(self):
        pack = self.generate_packdef("""
---
name: moddef
mods:
  - somemod:
      extrafield: "blah"
""")
        assert isinstance(pack, packdef.PackDefinition)
        mod = pack.get_mod('somemod')
        assert isinstance(mod, packdef.ModDefinition)
        with pytest.raises(AttributeError):
            assert mod.extrafield is None

    def test_moddefinition_latest_version(self):
        pack = self.generate_packdef("""
---
name: moddef
mods:
  - somemod
""")
        assert isinstance(pack, packdef.PackDefinition)
        mod = pack.get_mod('somemod')
        assert isinstance(mod, packdef.ModDefinition)
        assert mod.version == "latest"

    def test_moddefinition_version_compat(self):
        pack = self.generate_packdef("""
---
name: moddef
mods:
  - somemod:
      version: "somemod-1.2.3.jar"
""")
        assert isinstance(pack, packdef.PackDefinition)
        mod = pack.get_mod('somemod')
        assert isinstance(mod, packdef.ModDefinition)
        assert mod.version == "somemod-1.2.3.jar"

    def test_moddefinition_bad_recommendation(self):
        with pytest.raises(OperationError):
            self.generate_packdef("""
---
name: moddef
mods:
  - somemod:
      recommendation: maybe
""")

    def test_moddefinition_raw_url(self):
        pack = self.generate_packdef("""
---
name: moddef
mods:
  - somemod:
      url: "https://www.somesite.com/path/to/mod.jar"
""")
        assert isinstance(pack, packdef.PackDefinition)
        mod = pack.get_mod('somemod')
        assert isinstance(mod, packdef.ModDefinition)
        assert mod.url == 'https://www.somesite.com/path/to/mod.jar'

##############################################################################
# THE END
