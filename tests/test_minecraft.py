# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from packmaker.download import HttpDownloader
from packmaker.minecraft import Minecraft

##############################################################################


class Base_TestMinecraft (object):
    mc_version = '__undefined__'

    @pytest.fixture(scope='function')
    def minecraft_obj(self):
        mc = Minecraft(self.mc_version)
        yield mc

    def test_manifest(self, minecraft_obj):
        manifest = minecraft_obj.get_version_manifest()
        assert manifest is not None
        for elem in ['assetIndex', 'assets', 'downloads', 'id', 'libraries',
                     'logging', 'mainClass', 'minimumLauncherVersion',
                     'releaseTime', 'time', 'type']:
            assert elem in manifest
        assert manifest['id'] == self.mc_version

    def test_assets_index(self, minecraft_obj):
        index = minecraft_obj.get_assets_index()
        assert index is not None
        assert 'objects' in index

    def test_libraries(self, minecraft_obj):
        libraries = minecraft_obj.get_libraries()
        assert libraries is not None

    @pytest.mark.slow
    def test_download_server(self, minecraft_obj, tmp_path):
        downloader = HttpDownloader(tmp_path)
        minecraft_obj.download_server(downloader)

    @pytest.mark.slow
    def test_download_client(self, minecraft_obj, tmp_path):
        downloader = HttpDownloader(tmp_path)
        minecraft_obj.download_client(downloader)

##############################################################################


class TestMinecraft1_7_10 (Base_TestMinecraft):
    mc_version = '1.7.10'


class TestMinecraft1100 (Base_TestMinecraft):
    mc_version = '1.10'


class TestMinecraft1101 (Base_TestMinecraft):
    mc_version = '1.10.1'


class TestMinecraft1102 (Base_TestMinecraft):
    mc_version = '1.10.2'


class TestMinecraft1110 (Base_TestMinecraft):
    mc_version = '1.11'


class TestMinecraft1111 (Base_TestMinecraft):
    mc_version = '1.11.1'


class TestMinecraft1112 (Base_TestMinecraft):
    mc_version = '1.11.2'


class TestMinecraft1120 (Base_TestMinecraft):
    mc_version = '1.12'


class TestMinecraft1121 (Base_TestMinecraft):
    mc_version = '1.12.1'


class TestMinecraft1122 (Base_TestMinecraft):
    mc_version = '1.12.2'


class TestMinecraft1130 (Base_TestMinecraft):
    mc_version = '1.13'


class TestMinecraft1131 (Base_TestMinecraft):
    mc_version = '1.13.1'


class TestMinecraft1132 (Base_TestMinecraft):
    mc_version = '1.13.2'


class TestMinecraft1140 (Base_TestMinecraft):
    mc_version = '1.14'


class TestMinecraft1141 (Base_TestMinecraft):
    mc_version = '1.14.1'


class TestMinecraft1142 (Base_TestMinecraft):
    mc_version = '1.14.2'


class TestMinecraft1143 (Base_TestMinecraft):
    mc_version = '1.14.3'


class TestMinecraft1144 (Base_TestMinecraft):
    mc_version = '1.14.4'


class TestMinecraft1150 (Base_TestMinecraft):
    mc_version = '1.15'


class TestMinecraft1151 (Base_TestMinecraft):
    mc_version = '1.15.1'


class TestMinecraft1152 (Base_TestMinecraft):
    mc_version = '1.15.2'


class TestMinecraft1160 (Base_TestMinecraft):
    mc_version = '1.16'


class TestMinecraft1161 (Base_TestMinecraft):
    mc_version = '1.16.1'


class TestMinecraft1162 (Base_TestMinecraft):
    mc_version = '1.16.2'


class TestMinecraft1163 (Base_TestMinecraft):
    mc_version = '1.16.3'


class TestMinecraft1164 (Base_TestMinecraft):
    mc_version = '1.16.4'


class TestMinecraft1165 (Base_TestMinecraft):
    mc_version = '1.16.5'


##############################################################################
# THE END
