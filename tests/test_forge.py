# vim:set ts=4 sw=4 et nowrap syntax=python ff=unix:
#
# Copyright 2020 Mark Crewson <mark@crewson.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest

from packmaker.download import HttpDownloader
from packmaker.forge import ForgeModLoader
from packmaker.framew.log import getlog

##############################################################################


def setup_module(module):
    log = getlog()
    log.setLevel('DEBUG')


class Base_TestForge (object):
    mc_version = '__undefined__'
    forge_version = '__undefined__'

    install_profile_elements = []
    version_info_elements = []

    can_have_no_installer_libraries = False

    @pytest.fixture(scope='function')
    def forge_obj(self, tmp_path):
        downloader = HttpDownloader(tmp_path)
        forge = ForgeModLoader(self.mc_version, self.forge_version, downloader)
        yield forge

    def test_install_profile(self, forge_obj):
        install_profile = forge_obj._get_install_profile()
        assert install_profile is not None
        for elem in self.install_profile_elements:
            assert elem in install_profile

    def test_install_profile_singleton(self, forge_obj):
        install_profile_1 = forge_obj._get_install_profile()
        install_profile_2 = forge_obj._get_install_profile()
        assert id(install_profile_1) == id(install_profile_2)

    def test_version_info(self, forge_obj):
        version_info = forge_obj._get_version_info()
        assert version_info is not None
        for elem in self.version_info_elements:
            assert elem in version_info

    def test_version_info_singleton(self, forge_obj):
        version_info_1 = forge_obj._get_version_info()
        version_info_2 = forge_obj._get_version_info()
        assert id(version_info_1) == id(version_info_2)

    def test_get_dependent_libraries(self, forge_obj):
        dependents = forge_obj.get_libraries()
        assert dependents is not None
        assert len(dependents) > 0

    def test_get_installer_libraries(self, forge_obj):
        dependents = forge_obj._get_installer_libraries()
        assert dependents is not None
        if not self.can_have_no_installer_libraries:
            assert len(dependents) > 0

    @pytest.mark.slow
    def test_install_client(self, forge_obj, tmp_path):
        cache_loc = tmp_path / 'cache'
        inst_loc = tmp_path / 'install'
        forge_obj.install(cache_loc, inst_loc, 'client')

    @pytest.mark.slow
    def test_install_server(self, forge_obj, tmp_path):
        cache_loc = tmp_path / 'cache'
        inst_loc = tmp_path / 'install'
        forge_obj.install(cache_loc, inst_loc, 'server')

##############################################################################


class Base_TestForge_1710 (Base_TestForge):
    mc_version = '1.7.10'
    install_profile_elements = ['install', 'versionInfo']
    version_info_elements = ['id', 'type', 'mainClass', 'minecraftArguments',
                             'inheritsFrom', 'libraries']
    can_have_no_installer_libraries = True


class Base_TestForge_1122_old (Base_TestForge):
    mc_version = '1.12.2'
    install_profile_elements = ['install', 'versionInfo', 'optionals']
    version_info_elements = ['id', 'type', 'mainClass', 'minecraftArguments',
                             'inheritsFrom', 'libraries']


class Base_TestForge_1122_new (Base_TestForge):
    mc_version = '1.12.2'
    install_profile_elements = ['libraries', 'processors', 'data', 'version']
    version_info_elements = ['id', 'type', 'mainClass', 'minecraftArguments',
                             'inheritsFrom', 'libraries']


class Base_TestForge_1130_new (Base_TestForge):
    install_profile_elements = ['libraries', 'processors', 'data', 'version']
    version_info_elements = ['id', 'type', 'mainClass', 'arguments',
                             'inheritsFrom', 'libraries']

##############################################################################


class TestForge_1710_1558 (Base_TestForge_1710):
    forge_version = '10.13.4.1558'


class TestForge_1122_2847 (Base_TestForge_1122_old):
    forge_version = '14.23.5.2847'
    can_have_no_installer_libraries = True


class TestForge_1122_2854 (Base_TestForge_1122_new):
    forge_version = '14.23.5.2854'


class TestForge_1152_31200 (Base_TestForge_1130_new):
    mc_version = '1.15.2'
    forge_version = '31.2.0'


class TestForge_1152_31231 (Base_TestForge_1130_new):
    mc_version = '1.15.2'
    forge_version = '31.2.31'


class TestForge_1161_32063 (Base_TestForge_1130_new):
    mc_version = '1.16.1'
    forge_version = '32.0.63'


class TestForge_1161_320108 (Base_TestForge_1130_new):
    mc_version = '1.16.1'
    forge_version = '32.0.108'


class TestForge_1162_3303 (Base_TestForge_1130_new):
    mc_version = '1.16.2'
    forge_version = '33.0.3'


class TestForge_1163_34142 (Base_TestForge_1130_new):
    mc_version = '1.16.3'
    forge_version = '34.1.42'


class TestForge_1164_35113 (Base_TestForge_1130_new):
    mc_version = '1.16.4'
    forge_version = '35.1.13'

##############################################################################
# THE END
