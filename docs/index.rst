Packmaker  
=========

Packmaker is a command line tool used to build Modded Minecraft modpacks on
Linux systems. It can take a yaml-based description of a pack and generate
a package suitable for use in the following formats:

* **Curseforge** - A curseforge package suitable for uploading directly to the
  curseforge.com webiste, or importing into a curseforge compatible minecraft
  launcher like MultiMC or the Twitch Client.

* **Routhio** - A package format used by the minecraft.routh.io community and the
  routh.io minecraft launcher.

* **Local client** - A local minecraft client installation, suitable for standalone
  games.  Packmaker can even launch your modded minecraft installation in
  offline mode itself. Great for testing your modpack during development.

* **Server** - A minecraft server installation, suitable for use by clients to
  connect with and play together.

If you're new to packmaker, begin with the :doc:`guides/main` guide. That
guide walks you through installing packmaker, setting it up how you like it,
and starting to build your first modpack.

Then you can get a more detailed look at packmaker's features in the
:doc:`/reference/cli/` and :doc:`/reference/config` references.

If you still need help, you can drop by the ``#packmaker`` channel on our
`Discord server`_, or `file a bug`_ in the issue tracker. Please let us know
where you think this documentation can be improved.

And if you're really ambitous, `checkout the code`_ and help us improve
packmaker for you and everyone.

.. _Discord server: https://discord.gg/TwJz9rZ
.. _file a bug: https://gitlab.routh.io/minecraft/tools/packmaker/issues
.. _checkout the code: https://gitlab.routh.io/minecraft/tools/packmaker


Contents
--------

.. toctree::
   :maxdepth: 2

   guides/index
   reference/index
   faq

.. toctree::
   :maxdepth: 1

   changelog
