Packmaker Definition Files
==========================

Minecraft modpacks built using Packmaker are defined in a YAML file, typically
called ``packmaker.yml`` [#]_.

The ``packmaker.yml`` file defines all of the metadata about a modpack, title,
version, authors, etc, and all of the details about what is needed to be
installed in a Minecraft instance to create the modpack. This include things
like what version of Minecraft, what version of Forge, and any mods and/or
resourcepacks to add to the instance.

There is an simple example of a ``packmaker.yml`` for a simple modpack in the
packmaker `git repository`_.


.. [#] ``packmaker.yml`` is the default name used for the this yaml file, and
   this reference document assumes the default filename is used, but you can
   use any filename you like, as long as you provide it on the command line.

.. _git repository: https://gitlab.routh.io/minecraft/tools/packmaker/-/tree/master/example

Introduction
------------
Packmaker definition files are written in YAML syntax.

YAML is used because it is easier for humans to read and write than other common data formats like
XML or JSON.

The definition file can be broken down into a number of different sections:

metadata
  All the pack metadata, including name, version, author, etc. Most of the metadata is
  required to properly define a modpack.

mods
  The list of mods to be installed in the modpack.  Mods are optional, and if this section is
  omitted, no mods will be installed. (But what would a modpack be without mods?)

resourcepacks
  The list of resourcepacks to be installed in the modpack. Like mods, resourcepacks are
  optional, and if not are needed, this section can be omitted.

files
  The list of additional files to be installed in the modpack. Again, files are optional and
  can this section can be omitted if there are no files to install.


The following example packmaker definition file defines every single element, and every parameter
for mods, resourcepacks, and files at least once.
::

    ---
    name: examplepack
    title: Example ModPack
    version: 1.0.0
    author:
      - mcrewson
      - crouth

    minecraft: 1.12.2
    forge: 14.23.5.2854

    releasetypes:
      mods:
        - beta
        - release
      resourcepacks:
        - alpha
        - beta
        - release

    mods:
      - jei:
      - journeymap:
          curseforgeid: 32274
          release: latest
      - the-one-probe
          ingoreddependencies:
            - mantle
      - tinkers-construct:
          release: TConstruct-1.12.2-2.13.0.183.jar
          clientonly: false
          serveronly: false
          optional: false
          recommendation: starred
          selected: false

    resourcepacks:
      - faithful-x32:
          release: Faithful 1.15.2-r1
          optional: false
          recommendation: starred
          selected: false

    files:
      - location: src
        clientonly: false
        serveronly: false

    routhio:
      html:
        - location: html
      launch:
        flags:
          - "-Dfml.ignoreInvalidMinecraftCertificates=true -XX:+UseG1GC -XX:UseSSE=3"
    ...

Metadata
--------
Modpack metadata is needed to properly identify the modpack

:name: The name of the modpack.  Currently used as part of the final filename for the released modpack file.
  Typically this should be short and simple, with no spaces or punctuation in the name, for better
  filenames.

  Required.

:title: The title of the modpack. This should be the full title of the pack.  For example, the title is
  what is used in a curseforge manifest file, so when you import your into twitch or multimc, this
  is the default name it will appear as.

  Required.

:version: The version of the modpack. Can be any string, but would recommend your follow a consistent versioning
  pattern (`semantic versioning`_ is a good example). Incrementing the version of a pack is how launchers are 
  able to detect and upgrade a modpack.

  Version is also used in building the final filename for the released modpack file.

  Required.

:authors: A list of names your want to credit with the creaton of the modpack. If only one name is needed, you
  can specify this as a string, rather than a list.

  Some modpack formats, the curseforge format in particular, do not support more than one author in
  their metadata. When building modpacks in those formats, packmaker only uses the first name in
  the list.
  
  Required.

:minecraft: This is the version of minecraft this pack is being built for. Any valid version string that
  Mojang has defined for Minecraft should be valid here, although it is recommended you use some of the
  more common versions:

  * 1.7.10
  * 1.10.2
  * 1.11.2
  * 1.12.2
  * 1.13.2
  * 1.14.4
  * 1.15.2

  This field is required.

:forge: The version of the Forge modloader that the pack is being built for.  Any valid version string
  that Forge has defined should be valid here, although it is recommended you use the latest for recommended
  version as defined by Forge. In fact, if you use the value 'recommended' or 'latest' for this version,
  packmaker will calculate the actual version based on what minecraftforge.net has defined for
  the version of minecraft you are using.

  This is an optional field.  If not defined, your modpack will not include Forge, most likely
  resulting in a vanilla minecraft pack, one with no mods loaded in it.

  The forge and fabric modloaders should not both be defined in the pack definition. That would be crazy.

:fabric: The version of the Fabric modloader that the pack is being built for. Any valid version string
  the Fabirc has defined should be valid here, although it is recommended you use the latest version as
  defined by Fabric.  In fact, if you use the value 'latest' for this version, packmaker will calculate
  the actual version based on what fabricmc.net has defined for the version of minecraft you are
  using.

  This is an optional field. If not defined, the Fabric modloader will not be included, most likely
  resulting in a vanilla minecraft pack, or at least one that cannot load Fabric mods.

  The forge and fabric modloaders should not both be defined in the pack definition. That leads to
  madness.

.. _semantic versioning: https://semver.org/

Environment Variables in Metadata
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Packdef metadata can be specified using environment variable syntax, and packmaker will use the
values of the environment variable when building the pack.  For example:
::

    ----
    name: examplepack
    title: Example ModPack
    version: ${VERSION:-development}

The ``version`` of this pack will be set at build time to the value of the ``VERSION`` environment
variable.  If the ``VERSION`` environment variable is not defined, the value ``development`` will
be used a fallback.

Environment variables are only valid for the metadata items in the packdef files. They will not
be interpolated in other parts of the packdef such as the ``mods`` or ``resourcepacks`` lists.

Release Types
-------------
Release types are a way to control the versions or releases of the mods and resourcepacks installed
in the pack.  Every version of a mod or resourcepack in Curseforge is classified by a release type.
While the meaning of these release types can be a little vague, and ultimately is up to the mod
authors, the three available release types typically mean the following:

  * ``apha`` : An alpha release is considered the least stable version of a mod. Testing may have not been
    completed and there may be game breaking bugs.
  * ``beta`` : A beta release is the middle ground, more stable than alpha, less than release. More testing
    has been done, but there may be some smaller issues still to be worked out.
  * ``release`` : The most stable of the release types, a "released" mod or resourcepack is has been deemed
    by the mod author as ready for everyone to use. Testing and most bugs should be worked out.

Typically, although not universally, and not guaranteed, a mod author will make many alpha releases of his
project while developing his code. When he decides that it is ready for use by a larger audience, he may
make a beta release or two, which fixes the bugs found be his alpha testers.  Eventually, once the mod
author is happy with the testing and is certain is mod or resourcepack is ready, he releases a final "release"
version of his project.

As the modpack author, you can specify which types of releases you want to include in your pack.  If you
specify nothing, packmaker will find and include the most recent (the "latest") version of the mods and
resourcepacks specified in your pack definition file, regardless of their release type.  If you do not
want to include "alpha" and/or "beta" versions in your modpack, you can control that by specifying which
release types are acceptable.

The ``releasetypes`` element in the pack definition can control this for all mods and resourcepacks in
your pack:
::

    releasetypes:
      mods:
        - alpha
        - beta
        - release
      resourcepacks:
        - alpha
        - beta
        - release

The allowed release types for mods and resourcepacks are specified separately (so you could allow "alpha" resourcepacks
but not "alpha" mods). For each, you can specify a list of allowed release types. This list will be used for
*ALL* of the mods or resourcepacks.  You can override this default releasetype value for individual mod and resourcepacks
by specifying a ``release`` parameter in the mod or resourcepack definition (see below).

It is allowed, although maybe a little odd, to specify unusual combinations of releasetypes, and packmaker
will respect it.  For example, your can specify that you want "alpha" and "release", but not "beta" types,
and packmaker will install the latest version of a mod that is either an alpha or a release, but not a beta
(even if there is a newer version of the mod that was releases as a beta).

Mod Definitions
---------------

``mods`` is a list of minecraft mod definitions.  Each definition requires the *slug name* of the mod,
as found on the `curseforge.com`_ site.

You can find the *slug name* of a mod using packmaker's search function.  Search results will contain
both the full name of a mod and the *slug name*.

The *slug name* of a mod on curseforge can also be found in the mod's address on the site. For example,
the curseforge page for the `Tinker's Construct`_ mod is:

    \https://www.curseforge.com/minecraft/mc-mods/tinkers-construct

The last part of the address, "**tinkers-construct**", is the *slug name* for this mod.

Each mod listed in this ``packmaker.yml`` file can be specified as either a string value, if no additional
parameters are required for the mod, or as a dictionary, where you can supply additional parameters to
control the specific version of the mod and other aspects of it within the modpack.

All of these parameters are optional.

:curseforgeid: An integer value, specifying the exact mod to be installed, based on its Curseforge ID
  in the curseforge api.  When this parameter is used, packmaker will not search for the mod in
  curseforge using the slug name specified, but instead assume the id specified here is exactly the one
  wanted, and no further checks will be done to verify it.

  This can be useful in some situations, where the curseforge search api is not returning the correct
  mod or cannot find it at all, but should be used as a last resort it will circumvent some of
  packmaker's safety checks. For example, it will not ensure the mod is compatible with the chosen
  modloader or minecraft version.

  Optional, and undefined by default.  Meaning, the slug name of the mod specified will be used to
  find the requested mod in curseforge.

:release: The specific release of the mod to be installed into the modpack. If not defined, packmaker
  will default to using the latest version of the mod available for the version of minecraft that the
  pack is built for.

  The ``release`` parameter must contain either the entire filename of the version of the mod desired, usually
  as specified on the curseforge site for the mod, or the curseforge id of the release. The curseforge id
  of the release can be useful if the curseforge search api is not find the exact mod you want.

  Alternatively, one of the following "magic" values can be used to have packmaker figure out which version
  be installed, based on the release types specified by the curseforge api:

  * ``latest-alpha`` : install the latest version of the mod with a release type of "alpha", "beta", or "released"
  * ``latest-beta`` : install the latest version of the mod with a release type "beta" or "released". "Alpha" releases will be ignored.
  * ``latest-release`` : install the latest version of the mod with a release type of "released". "Alpha" and "releases" will be ignored.
  * ``latest`` : install the latest version of the mod with release type as specified in ``releasetypes`` packdef element.

  Using these "magic" release values overrides the ``releasetypes`` specified globally (see above)
  for this mod.

  Optional, and defaults to ``latest``, meaning the version of the mod installed will be the latest
  available, as allowed by the packmaker.conf configuration file.

:url: To include a mod that cannot be found on Curseforge in a modpack, there are two options. You can
  include it directly in the pack as a file (see below), which works great but will greatly increase
  the size of your modpack and potentially create a licensing problem from mod owners that prefer their
  mods only be installed from their site.  The alternative is to specify a ``url`` for the mod.

  If the ``url`` parameter is used, it must be a complete and accurate download url for the mod jar file.
  Packmaker will then include only this url in your modpack (except for curseforge modpacks, see the next
  paragraph for that caveat), and a client launcher will be able to download the mod for the player
  directly from this url.  This means the modpack doesn't need to contain the large mod jar file, and
  the mod owner gets the download directly from their site.

  Caveat, Curseforge modpacks do not have a concept of downloading a mod from just a url, so packmaker
  will download and include the actual mod jar file into your curseforge modpack when building it. So the
  mod can still be defined in the packdef file with this url parameter, and packmaker will build it properly
  for all pack types.

  It should be noted that when the ``url`` parameter is used, all release and version checking that
  packmaker normally does to ensure a mod is compatible with your modloader and minecraft is ignored.

:clientonly: A boolean value, indicating whether this mod is specific to the minecraft client. If true,
  packmaker will not install this mod into any server builds.

  Optional, and defaults to false if not specified, meaning the mod will be installed in both client
  and server builds.

:serveronly: A boolean value, indicating whether this mod is specific to the minecraft server. If true,
  packmaker will only install this mod in server builds.

  Optional, and default to false if not specified, meaning the mod will be installed in both client
  and server builds.

:ignoreddependencies: Either the string "``all``", or a list of mods. This parameter specifies mods
  to be ignored when trying to resolve the dependencies of a mod.  If the value of this parameter
  is ``all``, no mod dependency resolution will be done for this mod.  Otherwise if a mod's
  dependency is part of this list, it will not be added to the set of mods in the pack as part of
  the mod dependency resolution process.

  Optional, and defaults to an empty list, meaning all dependent mods will be added to the pack
  automatically.

:optional: An optional parameter, only used for routhio builds.  This will mark the mod as optional,
  and the routhio launcher will present it as an optional mod that can be installed but is not
  required to launch the modpack.

  When building servers and other types of modpacks, for example curseforge modpacks, packmaker 
  will ignore this parameter.

:recommendation: An optional parameter, only used for routhio builds. This will mark an optional
  mod as recommended, marking the mod in the launcher as a recommended optional mod or an optional
  mod to avoid. Should have the value `starred` or `avoid`. Any other value will not make sense 
  to the routhio launcher and will cause the mod to be ignored.  Not including this parameter
  on an optional mod will make the launcher neither recommend to install nor avoid it.

  When building servers and other types of modpacks, for example curseforge modpacks, packmaker 
  will ignore this parameter.

:selected: An optional parameter, only used for routhio builds. This will mark an optional
  mod as selected, making the optonal mod installed by default, unless the user explicitly
  chooses not to.  Should have the value a boolean value, true or false.

  When building servers and other types of modpacks, for example curseforge modpacks, packmaker 
  will ignore this parameter.

.. _curseforge.com: https://www.curseforge.com/minecraft/mc-mods
.. _Tinker's Construct:  https://www.curseforge.com/minecraft/mc-mods/tinkers-construct

Resourcepack Definitions
------------------------

Resourcepacks are always minecraft client only elements.  They will be
ignored when building servers for the pack.  So there is no need for
``clientonly`` or ``serveronly`` parameters for a resourcepack.

:curseforgeid: An integer value, specifying the exact resourcepack to be installed, based on its Curseforge ID
  in the curseforge api.  When this parameter is used, packmaker will not search for the resourcepack in
  curseforge using the slug name specified, but instead assume the id specified here is exactly the one
  wanted, and no further checks will be done to verify it.

  This can be useful in some situations, where the curseforge search api is not returning the correct
  resourcepack or cannot find it at all, but should be used as a last resort it will circumvent some of
  packmaker's safety checks. For example, it will not ensure the resourcepack is compatible with the chosen
  modloader or minecraft version.

  Optional, and undefined by default.  Meaning, the slug name of the resourcepack specified will be used to
  find the requested resourcepack in curseforge.

:release: The specific release of the resourcepack to be installed into the modpack. If not defined,
  packmaker will default to using the latest version of the resoourcepack available for the version 
  of minecraft that the pack is built for.

  The ``release`` parameter must contain either the entire filename of the version of the resourcepack desired, usually
  as specified on the curseforge site for the resourcepack, or the curseforge id of the release. The curseforge id
  of the release can be useful if the curseforge search api is not find the exact resourcepack you want.

  Alternatively, one of the following "magic" values can be used to have packmaker figure out which version
  be installed, based on the release types specified by the curseforge api:

  * ``latest-alpha`` : install the latest version of the resourcepack with a release type of "alpha", "beta", or "released"
  * ``latest-beta`` : install the latest version of the resourcepack with a release type "beta" or "released". "Alpha" releases will be ignored.
  * ``latest-release`` : install the latest version of the resourcepack with a release type of "released". "Alpha" and "releases" will be ignored.
  * ``latest`` : install the latest version of the resourcepack with release type as specified in ``releasetypes`` packdef element.

  Using these "magic" release values overrides the ``releasetypes`` specified globally (see above)
  for this resourcepack.

  Optional, and defaults to ``latest``, meaning the version of the resourcepack installed will be the latest
  available, as allowed by the packmaker.conf configuration file.

  The ``release`` parameter must contain the entire filename of the version of the resourcepack
  desired, usually as specified on the curseforge site for the resourcepack.

  The value ``latest`` can also be used to explicitly specify that the latest version be installed.

:url: To include a resourcepack that cannot be found on Curseforge in a modpack, there are two options. You can
  include it directly in the pack as a file (see below), which works great but will greatly increase
  the size of your modpack and potentially create a licensing problem from resourcepack owners that prefer their
  resourcepack only be installed from their own site.  The alternative is to specify a ``url`` for the resourcepack.

  If the ``url`` parameter is used, it must be a complete and accurate download url for the resourcepack zip file.
  Packmaker will then include only this url in your modpack (except for curseforge modpacks, see the next
  paragraph for that caveat), and a client launcher will be able to download the resourcepack for the player
  directly from this url.  This means the modpack doesn't need to contain the large resourcepack zip file,
  and the resourcepack owner gets the download directly from their site.

  Caveat, Curseforge modpacks do not have a concept of downloading a resourcepack from just a url, so packmaker
  will download and include the actual resourcepack zip file into your curseforge modpack when building it. So the
  resourcepack can still be defined in the packdef file with this url parameter, and packmaker will build it properly
  for all pack types.

  It should be noted that when the ``url`` parameter is used, all release and version checking that
  packmaker normally does to ensure a resourcepack is compatible with your modloader and minecraft is ignored.

:optional: An optional parameter, only used for routhio builds.  This will mark the resourcepack
  as optional, and the routhio launcher will present it as an optional resourcepack that can be 
  installed but is not required to launch the modpack.

  When building other types of modpacks, for example curseforge modpacks, packmaker will ignore this
  parameter.

:recommendation: An optional parameter, only used for routhio builds. This will mark an optional
  resourcepack as recommended, marking the resourcepack in the launcher as a recommended optional
  resourcepack or an optional resourcepack to avoid. Should have the value `starred` or `avoid`.
  Any other value will not make sense to the routhio launcher and will cause the resourcepack to
  be ignored.  Not including this parameter on an optional resourcepack will make the launcher
  neither recommend to install nor avoid it.

  When building other types of modpacks, for example curseforge modpacks, packmaker will ignore this
  parameter.

:selected: An optional parameter, only used for routhio builds. This will mark an optional
  resourcepack as selected, making the optonal resourcepack installed by default, unless the user explicitly
  chooses not to.  Should have the value a boolean value, true or false.

  When building other types of modpacks, for example curseforge modpacks, packmaker will ignore this
  parameter.

Files
-----
The files section of the ``packmaker.yml`` file specifies the local files to be included into the
modpack.  Typically these would be an additional configurations, scripts, resources, etc that
you want to add to the minecraft install when it is installed.

This section is a list of file locations, typically subfolders or directories within your packmaker
project. Each location in the list should be unique and not overlap another location, as all
locations will be combined in the final poduct.

The contents of each location folder will be copied into the minecraft folder of the built instance,
with no alterations or attempt to preserve what is already stored in the destination, so be
care not to accidently overwrite any files that may have already been installed by minecraft
itself or mod installations.

A typical use for multiple file locations is to specify some files be installed only on client
builds, and other only installed on server builds. For example:
::

    files:
      - location: src-all
      - location: src-client
        clientonly: true
      - location: src-server
        serveronly: true
      - location: templates
        template: true

Each location in the files list can have the following parameters.

:location: The location, relative to the ``packmaker.yml`` file itself, of the local files to
  be included in the modpack.

  If this location does not physically exist when building the modpack, a warning is printing,
  by this location will be ignored.

:clientonly: An optional boolean flag (true or false), indicating that this location of files
  should only be included with client builds.

  Defaults to false, meaning the location will be included in both client and server build,
  unless the corresponding ``serveronly`` parameter is specified.

:serveronly: An optional boolean flag (true or false), indicating that this location of files
  should only be included with server builds.

  Defaults to false, meaning the location will be included in both client and server build,
  unless the corresponding ``clientonly`` parameter is specified.

:template: An optional boolean flag (true or false), indicating that this location of files
  are Jinja2 templates.  Rather than simply copying files from this location to the instance
  destination, Packmaker will load and render these files using the Jinja2 templating system.
  The packdef metadata (name, title, version, minecraft_version, etc) can be used as template
  variables inside each template file however you want.

  This can be useful for rendering pack data, for example the pack title and version, into
  mod configuration files, such as custom main menu text.

  Defaults to false, meaning the files at this location are not templates, but plain files
  that will be copied to an instance with no modifications.


Routhio
-------
The routhio section of the ``packmaker.yml`` file is specific to only one type of modpack build,
a modpack for the routhio launcher. All other build types will ignore this section.

This section includes the following elements:

:html: A location of additional html files, typically a local folder with the project, that will 
  be displayed within the launcher when a user selected this specific modpack.

:launch: Additional options the launcher will use when launching a minecraft instance. Currently,
  the only launcher option that can be specified here is ``flags``, which is used to provide 
  additional java command line arguments on the minecraft process.

