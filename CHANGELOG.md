
# Changelog

Notable changes to Packmaker are documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9.2] - 2021-08-23

### Changed

- Reduced that pagesize of search queries to the curseforge api from 1000 to 50.  This appears
  to be a forced change required by Curseforge/Overwolf, because any query with a larger
  pagesize now returns a 400 error.  This will severly slow down large queries, such as those
  used by the `updatedb` cmd.

- Fix builds of curseforge modpacks which include non-curseforge addons (Spacerace is a good
  example of such a modpack).  Building such a pack was throwing errors rather than adding
  the non-curseforge addon to the overrides folder. Oops.


## [0.9.1] - 2021-03-27

### Changed

- Fuzzy minecraft versions. Nearly all addons for minecraft version 1.16.4 also work fine for
  minecraft version 1.16.5, and vice versa.  However, up until now packmaker would only resolve
  exact versions of minecraft in a pack (i.e., an addon had to explicitly state it supported 
  version 1.16.5 before packmaker would resolve it for a 1.16.5 based modpack).  This change 
  solves this problem, as packmaker now knows about this "fuzzy" relationship between minecraft
  versions will find addons for both versions when trying to resolve an addon.

  This fuzzy support is currently only defined for minecraft version 1.16.4 and 1.16.5, but
  other similar versions can be added easily enough when needed.

  There is also a command line flag, `--strict` to disable fuzzy version resolving if needed.

- Fixed inclusion of html files in the routhio builder, so hopefully no more missing pack news
  pages for packs in the routhio launcher.

- Caught and properly report another instance of when the packdef yaml file is syntactically
  correct, but makes no sense to packmaker itself.

- It is invalid if both the forge modloader and fabric modloader are specified in the packdef.
  Packmaker now properly reports this error condition.

- Updated the reference docs to include some of the newer options and packdef parameters that
  have been added to recent versions of packmaker.


## [0.9.0] - 2021-02-06

### Added

- SPEED! When download large bulk downloads, for example when download a long list of mods, or
  downloading all of the minecraft client assets, Packmaker now attempts to do these concurrently.
  This results in up to a 5x speed increase in running packmaker commands. In my tests, using
  `packmaker build-local` to build a local minecraft client of the example forge project went
  from taking approximately 2.5 minutes down to only 30 seconds.

- Added support for raw url definitions of addons. Usefil for when mods are not in curseforge.
  Now you can just specify their raw download url, rather than having to include them as a
  file in the modpack. Note that some modpack format, curseforge in particular, cannot handle
  raw urls, and so packmaker will download and include any addon with a raw url into the pack
  directly, basically automated what how you had to handle such mods anyway. Example:
  ```yaml
  ---
  mods:
  - galacticraftcore:
      url: https://micdoodle8.com/new-builds/GC-1.12/281/GalacticraftCore-1.12.2-4.0.2.281.jar
  - galacticraft-planets:
      url: https://micdoodle8.com/new-builds/GC-1.12/281/Galacticraft-Planets-1.12.2-4.0.2.281.jar
  - micdoodlecore:
      url: https://micdoodle8.com/new-builds/GC-1.12/281/MicdoodleCore-1.12.2-4.0.2.281.jar
  ```

- Jinja2 templates. A files location in the packdef can be designated as a collection of
  templates, and packmaker will run all the files in that location through jinja when copying
  them during a build:
  ```yaml
  ---
  files:
    - location: src/              # <-- regular files. copied verbatim
    - location: templates/        # <-- template files. rendered through jinja2
      jinja2: true
  ```
  The packdef metadata (name, title, version, minecraft_version, etc) can be used as template
  variables inside each template file however you want. This can be useful for rendering pack
  data, for example the pack title and version, into mod configuration files, such as custom
  main menu text.

- A new modpack format, and the packmaker cmdline tools to build it! `packmake build-pack`
  will generate this new modpack format.  Stay tuned for new tools that will be making use
  of this.

- Pack release files can be compressed using bz2 or Xz compression formats. Any pack format
  can be compressed in any format, although it doesn't make much sense to compress a
  curseforge pack as anything but a zip, as no launcher will be able to handle it.

### Changed

- fabric libraries now use the same download logic that was implemented for forge libraries
  in packmaker 0.8.1 (see below)

- cleaned up the caching locations of modloader downloads.  Builders (and the launch cmd) where
  downloading modloader urls and dependent libraries to multiple different cache locations. This
  sometimes meant downloading the same file multiple times. Now all are using the same cache
  locations consistently.

- some (most?) curse api calls do not require authentication tokens. Packmaker will no longer
  include the auth header for such calls.

- emit a nice error message rather than an ugly traceback when setup.py fails to run git. Addresses
  issue #23

- some rearrangement and additional example packs are now included in the packmaker source
  repository. Example forge and fabric packs make it easy to setup test scenarios.

- More unittests. 51% code coverage now. Yay!


## [0.8.1] - 2020-12-23

### Changed
- Fix the download/install of the latest MC 1.16.4 versions of the Forge modloader.

  Downloading of forge libraries is no longer "hardcoded" to try to use the (old)
  https://modloaders.cursecdn.com domain first, before falling back to minecraftforge.net.
  This broke downloads of the most recent versions of forge.  With curseforge moving
  to Overwolf, this old cdn domain is no longer being updated with the newer versions of
  forge.

  However, the Curseforge api does provide endpoints that give all the needed information
  to download forge (and its dependent libraries) from a Overworld cdn.  So packmaker is
  now using that to get Forge.  It still falls back to the original minecraftforge.net
  site if all other avenues fail.


## [0.8.0] - 2020-11-25

### Added
- For those difficult addons that searching the curseforge api just cannot seem to find
  (curse you, curseforge!) you can now specify the exact curseforge id of the desired addon,
  and packmaker will just use that, rather than search for it:
  ```
  ---
  mods:
    - abyssalcraft:
        curseforgeid: 53686
  ```
- Similar to the above, but for releases of an addon, your can specify the curseforge file
  id of a release of an addon to explicity define the release.  This can be helpful when
  a mod author reuses the same release name (and filename) for releases. For example, see
  recent releases of the Thaumcraft mod:
  ```
  mods:
    - thaumcraft
        release: 2629033
  ```
  Packmaker will treat any release that is an integer value as a curseforge file id.
- The beginnings of a new modpack output format ("build-pack").  Not for consumption yet...

### Changed
- The "updatedb" cmd now makes two passes through the curseforge api, building the list
  of addons to record in the local curseforgedb.  The first pass, grabs all of the 'popular'
  addons, the second grabs all of the 'last updated'.  The union of these two passes should
  generate a much larger set of addons in the curseforgedb, and include more of the commonly
  used addons. Meaning there should be fewer "Cannot find addon in local db" messages now.
  FTR, all of this is to work around a problem in the curseforge search api, where it
  refuses to return more than 10000 paged results in a search query. 10001 or more results
  in a 500 error response. :-(
- Documentation updates for the above, plus some of the other new features from the past
  few versions of packmaker (oops).

## [0.7.1] - 2020-10-18

### Changed
- Fix building 1.7.10 packs for Routhio launcher.  Some libraries for these older versions
  of minecraft do not supply their own download urls in the install profile. So the
  routhio builder now has to iterate through a list of potential urls until it finds one
  that is valid.
- Fixing server and local builds for 1.7.10 on Windows.  I forgot about the "${arch}"
  option allowed for certain native libraries, causing attempts to fetch libraries with
  "${arch}" in their name to break.

## [0.7.0] - 2020-10-17

### Added

- Fabric modloader support.  You can use Fabric rather than Forge now, and packmaker
  will attempt to download fabric versions of the mods (or resourcepacks):
  ```
  ---
  name:  examplepack
  title: Example Pack
  version: ${MODPACK_VERSION:-dev}
  authors:
    - mcrewson
  minecraft: 1.16.3
  fabric: latest
  mods:
    - applied-energistics-2
  ```
  Packmaker will report errors if the mod doesn't have a fabric version (and vice-versa,
  for trying to add a fabric mod to a forge pack).

### Changed

- Fixed packmaker for older version of Minecraft and Forge, especially v1.7.10.
  Referencing version of Forge these older version required a different file
  naming pattern.
- The packmaker project has migrated to a public gitlab.com project:
  https://gitlab.com/routhio/minecraft/tools/packmaker

## [0.6.2] - 2020-09-02

### Changed

- Fixed the routhio json manifest file for modpacks built on Windows.  The manifest
  must use unix-style, `/`, path separators for files regardless of the platform it
  was being built on.
- Fixed building routhio modpacks for versions of Minecraft 1.14 and higher.  The
  dependent libraries where no longer being calculated correctly due to a change
  in forge installer definitions.
- Packmaker will now emit a warning when attempting to build a routhio modpack for
  versions of Minecraft greater than 1.12.2.  The Routio launcher, or any launcher
  forker from SkCraft, cannot run these modpacks, even if packmaker will build them.
- Wrote the initial configuration file reference documentation.

## [0.6.1] - 2020-08-31

### Changed

- When parsing metadata from the pack definition file, ensure it is always treated as
  string values, even when the value is a number.  This is particularly bad in the
  'version' field, where a version like '1.0' was being parsed as a number and then
  used a string and throwing exceptions.

## [0.6.0] - 2020-08-22

### Added

- Alpha/Beta/Release tracking of addons.  The following new release/version tags can
  be used for individual addons, indicating that you want the latest version of an
  addon with at least this released status:
    - "latest-release": only addons with a "released" status are acceptable. Ignore
      alpha and beta releases for the addon.
    - "latest-beta": only addons with at least a "beta" status are acceptable. Ignore
      alpha releases for the addon.
    - "latest-alpha": addons of any status, "alpha", "beta", or "released" are acceptable.
      Do not ignore any releases for the addon. This is a synonym for "latest".
  The default is release/version of an addon when not specified is "latest", which will
  include any release status of an addon.
- A new top-level pack definition element has been added to control alpha/beta/release
  status tracking for all addons.
  ```
  releasetypes:
    mods:
      - alpha
      - beta
      - release
    resourcepacks:
      - alpha
      - beta
      - release
  ```
  This allows you to specify which release status you want to accept for **ALL** addons
  in your modpack. For example, if you omit "alpha" from the releasetypes of mods, no
  mod with the release status of "alpha" will be included in the pack, unless explicitly
  defined by that mod's release field.
- The version of the Forge modloader can be specifed as "latest" or "recommended" now,
  and packmaker will resolve that to the correct version, as defined on the Forge
  website, during the lock operation.

### Changed

- Restored the default logging level to INFO. The logging level of packmaker in the
  previous version was made too quiet.  Not enough information was sent to the console
  by default, often making it appear packmaker was sometimes doing nothing or stalled.
- Cleaned up more of the console output, for example, moving the launch cmd output to
  logging statement, so that it is consistently controllable by log levels.
- Packmaker catches and reports more yaml syntax errors in your pack definition file now.
  Hopefully, with better messaging indicating where in the yaml file the error is occuring.
- Improved packmaker's logic for where to look for its config file. For Unix/Mac, it
  will use the standard XDG\_CONFIG\_HOME environment variable, if defined, to look for
  its config file. Additinoally on Mac, it will look in "~/Library/Application Support".
  On Windows, it will check the APPDATA environment variable, and also try 
  "~\AppData\Roaming". All the usual locations are still checked as well, so any existing
  configuration should still work.
- Minor fixes related to config file parsing that probably no one (including me) has
  noticed was broken.  An artifact of taking an old python 2 module into this python 3
  project.
- Fix the error detection when Minecraft dies abnormally when `packmaker launch` is used
  to start the game.
- The local curseforge moddb, built by the "updatedb" cmd, is completely optional now.
  Still recommended, as it will significantly speed up the lock and other operations that
  rely on making curseforge api calls when the local db is not available.
- The version of an addon was not being recorded correctly in the lock file.
- Tested. Minecraft 1.16.2 and Forge 33 work with Packmaker.
- More unittests. Up to 35% code coverage now.

## [0.5.0] - 2020-08-01

### Added

- Mod dependency resolution.  Packmaker will check for mod dependencies and automatically
  add them to the pack if they are not defined in the pack definition yaml file. You
  don't need to list all those library mods in your pack definition anymore.
- Mod definitions have a new optional field, 'ignoreddependencies' (can be abbreviated to
  'ignoreddeps'). Use this to list any mod dependencies you know can be safely ignored
  and packmaker will not add them. Use 'ignoreddependencies: all' to completely disable
  dependency resolution for a mod.
- Windows support. While not the primary target operating system, Packmaker can be
  used on Windows (tested on Win10) to build packs now.

### Changed

- The global '-v', '--verbose' flag, and corresponding '-q', '--quit' flag, work properly
  again to raise/lower the verbosity of packmaker's output.
- The default output verbosity is has been lowered. Only warnings and errors are output by
  default now (Use `packmaker -v ...` if you want the old level of output).
- Fixed a problem with the Routhio builder and the latest version of Forge for 1.12, where
  it would not be able to find and download the 'universal' forge jar.
- Packmaker will warn if the 'name', 'title', 'version', or 'authors' metadata fields are
  not defined in your pack defintion yaml file (during the lock cmd).
- Cleaned up the error reporting when building curseforge modpacks with the above missing
  metadata fields. A proper error message, not a stacktrace is reported.
- The 'authors' metadata field in the pack definition yaml file can be shortened to
  'author' if you want.
- Unittests. The packmaker codebase is actually starting to get unittests now. Yay for
  stability (I hope).

## [0.4.3] - 2020-07-13

### Changed
- Fixed the `forge_jarfile` field in the info cmd.  This was not updated when the mechanics
  of how Forge is installed, and the jar filename was changed, and therefore breaking
  mine-init's ability to find and run newer packs.
- Switched base image used for the Packmaker image to the standard 'python:3-slim' image.
  Results in a much much smaller container.

## [0.4.2] - 2020-07-12

### Added

- `apidebug` cmd, which can be used to interact directly with the curseforge api
  from the command line. Not documented yet.

### Changed

- Documentation has moved to readthedocs.io:  https://packmaker.readthedocs.io
- Changed the name of the addon `version` field to `release`, which is more appropriate.
  `version` still works for backwards compatability.
- The addon `release` field can reference either the fileName or displayName of the file api.
- Specifying an addon `release` field will supercede any checks for valid releases of the
  addon, instead assuming you know what your doing.
- Try not to use Fabric mods in Forge modpacks (and vice versa).  When Forge is defined
  in a packdef file, packmaker will omit mods that are marked as for the Fabric
  modloader in the curseforge api. (When the Fabric modloader is supported by packmaker,
  the reverse will also be true.)
- Removed the useless build-specific cmd parameters from the `findupdates` and `info` commands.
- Added documentation about environment variable interpolation in packdef metadata.
- The help cmd (and '-h' option) no longer requires a configuration file to run.
- The `--dump` option will censor the output of the `curseforge::authentication_token` 
  config file parameter. Don't accidentally share security tokens, m'kay?

## [0.4.1] - 2020-06-27

### Changed

- Convert most raw exception to cleaner, hopefully more helpful, error messages.
- Fix the routhio builder for new versions of forge 1.12, where the installer.jar
  has changed.
- Fix a failure to find mods when resourcepacks of the same name also exist.
- Changes to the build pipeline so that versions are computed based off of git
  tags, and docker images will be built on changes to the 'next' branch (as well
  as tags on the 'master' branch).

## [0.4.0] - 2020-06-12

### Added

- Resourcepack support! The packdef yaml file supports defining dependent resourcepacks
  in your modpack:
  ```yaml
  ---
  resourcepacks:
    - faithful-32x:
  ```
  All of the various packmaker commands understand and can work with them:
    - `search` will return resourcepack results, along with mods.
    - `updatedb` will scan for and add new resourcepacks to the local curseforge db.
    - `lock` will add the defined resourcepacks to your packs lock file.
    - `build-xxx` will build your modpack, and include the resourcepacks in the release.
    - `convert` recognizes resourcepacks in curseforge modpacks and can convert them
      to packmaker yaml files.
    - `findupdates` will find new version of resourcepacks.
    - `resourcepacksinfo` is a new command, which is analagous to the existing `modsinfo`
      command, but for resourcepacks.
- The launch cmd can now optionally have its java arguments specified in the config file.
  For example:
  ```ini
  [launch]
  java_binary = /usr/lib/jvm/java-8-openjdk/bin/java
  java_memory = 8192m
  java_arguments = -XX:+UseG1GC -Dfml.readTimeout=180
  ```
- Reference documentation was added for the packdef yaml file.

### Changed

- The `findmodupdates` cmd was renamed to `findupdates`, as it can search for more than
  just new versions of mods now.
- The `--dump` cmdline option actually works now.
- Documentation was updated for new and changed commands.

## [0.3.3] - 2020-06-01

### Changed

- Added a check in setup.py for python 3.  Running setup via python 2 will now abort
  with an error message.
- Fix the updatedb command. Only attempt to create a missing directory for the curseforge
  db *if* there is a directory specified in the config parameter.

## [0.3.2] - 2020-06-01

### Added

- New command, `convert`, which can convert a curse/twitch modpack into a packmaker one.
  This command takes the curse pack's manifest.json as input and outputs a new
  packmaker.yml file, with all of the relavent details filled in, including all of the
  mods (and versions of those mods) correctly defined.

### Changed

- Fix forge installations for some older versions of forge. Ignore libraries in the install
  profile that do not specify a url, on the assumption that the lib is loaded from
  somewhere else, likely from minecraft's own install profile.
- Mod definitions in the packdef yaml file can be defined as a string, if no meta data
  about the mod is needed. This means the trailing colon (:) on the line is now optional.
- In build commands, only warn, not throw an exception, when locations in the
  `files` list are missing and ignore the location for the remainder of the build.
- In updatedb command, create the directory the curseforge db is kept in, if it does
  not already exist.

## Older versions

Sorry, no changelog exists for older versions of Packmaker.

[unreleased]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.9.2...next
[0.9.2]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.9.1...0.9.2
[0.9.1]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.9.0...0.9.1
[0.9.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.8.1...0.9.0
[0.8.1]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.8.0...0.8.1
[0.8.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.7.1...0.8.0
[0.7.1]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.7.0...0.7.1
[0.7.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.6.2...0.7.0
[0.6.2]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.6.1...0.6.2
[0.6.2]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.6.1...0.6.2
[0.6.1]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.6.0...0.6.1
[0.6.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.5.0...0.6.0
[0.5.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.4.3...0.5.0
[0.4.3]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.4.2...0.4.3
[0.4.2]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.4.1...0.4.2
[0.4.1]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.4.0...0.4.1
[0.4.0]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.3.2...0.3.3
[0.3.3]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.3.2...0.3.3
[0.3.2]: https://gitlab.com/routhio/minecraft/tools/packmaker/-/compare/0.3.1...0.3.2
